" Local variables
" define ToC pattern (see $VIMRC)
let b:toc_pattern = '^- ['

" Local settings
" wrap long lines
setlocal wrap
setlocal formatoptions-=c
setlocal formatoptions-=r
setlocal formatoptions-=o

" use gf to navigate included files
setlocal suffixesadd=.todo

" folding
setlocal foldlevel=0

" set local tab width
" number of spaces for a tab character
setlocal tabstop=2
" number of spaces for inserting a tab or a backspace character
setlocal softtabstop=2
" number of spaces for the indent command
setlocal shiftwidth=2

" set wrapping long lines at a character in 'breakat'
setlocal linebreak
setlocal showbreak=\ \ \ \ \ \ 

" set spelling
" setlocal spell spelllang=en_us,ru

" Local mappings
" insert
inoremap <buffer> <C-s> - [ ] 

" toggle
function! todo#Toggle() range
    let start = a:firstline
    let end = a:lastline
    for i in range(start, end)
        if getline(i) =~ '^\s*- [ '
            execute i . ',' . i . 's/^\(\s*\)- \[ \(.*\)/\1- [x\2/'
        elseif getline(i) =~ '^\s*- [x'
            execute i . ',' . i . 's/^\(\s*\)- \[x\(.*\)/\1- [>\2/'
        elseif getline(i) =~ '^\s*- [>'
            execute i . ',' . i . 's/^\(\s*\)- \[>\(.*\)/\1- [ \2/'
        endif
    endfor
endfunction
command! -range -buffer Toggle <line1>,<line2>call todo#Toggle()
nnoremap <silent> <buffer> <C-s> :Toggle<CR>
xnoremap <silent> <buffer> <C-s> :Toggle<CR>

" mark
function! todo#Mark() range
    let start = a:firstline
    let end = a:lastline
    for i in range(start, end)
        if getline(i) =~ ' // '
        execute i . ',' . i . 's/^\(.*\) \/\/.*$/\1/'
    elseif getline(i) !~ ' // '
        execute i . ',' . i . 's/^\(.*\)$/\1 \/\/ ' . strftime("%Y-%m-%d %H:%M") . '/'
    endif
    endfor
endfunction
command! -range -buffer Mark <line1>,<line2>call todo#Mark()
nnoremap <silent> <buffer> <Leader>m :Mark<CR>
xnoremap <silent> <buffer> <Leader>m :Mark<CR>

" sort
function! todo#Sort() range
    " make sure :SortGroup is installed: https://www.reddit.com/76m6xa/
    if !exists(':SortGroup')
        echoerr "ERROR: SortGroup command is not present"
        return
    endif
    let done = []
    " set the boundaries
    let start = a:firstline
    let end = a:lastline
    " save the current foldlevel
    let fold_level = &l:foldlevel
    " make sure all the folds are expanded
    let &l:foldlevel = 1
    " detect the maximum indent
    let m = max(map(range(1, end), 'indent(v:val)'))
    " sort the indented blocks from the highest indent towards the lowest one
    for j in range(m, 0, -&shiftwidth)
        " find the indented lines for that particular indent
        let l = filter(range(1, end), 'indent(v:val) >= j')
        " find the lesser indented lines for that particular indent, breakpoints
        let b = filter(range(1, end), 'indent(v:val) < j')
        " group lines into blocks according to breakpoints
        let rez = []
        let idx = 0
        for i in b
            let sublist = []
            while idx < len(l) && l[idx] < i
                call add(sublist, l[idx])
                let idx += 1
            endwhile
            if len(sublist) > 0
                call add(rez, sublist)
            endif
        endfor
        " if the indented blocks are present...
        if len(rez) > 0
            " ...eliminate the unit blocks (blocks containing only 1 element)...
            call filter(rez, 'len(v:val) > 1')
            " ...and iterate over remaining blocks sorting the inner lines:
            for i in rez
                " if haven't sorted before...
                if index(done, i) == -1
                    " ...sort and...
                    silent! execute i[0] . ',' . i[-1] . 'SortGroup /^\s\{' . indent(i[0]) . '}-/'
                    " save the boundaries in order not to sort again
                    call add(done, i)
                endif
            endfor
        endif
    endfor
    " and at last, sort the very top level blocks
    silent! execute start .',' . end . "SortGroup /^-/"
    " then restore &foldlevel and...
    let &l:foldlevel = fold_level
    " ...force re-apply it
    call feedkeys("zX")
endfunction
command! -range=% -buffer Sort <line1>,<line2>call todo#Sort()
inoremap <silent> <buffer> <F8> <Esc>:Sort<CR>
nnoremap <silent> <buffer> gs :Sort<CR>
nnoremap <silent> <buffer> <F8> :Sort<CR>
xnoremap <silent> <buffer> gs :Sort<CR>
xnoremap <silent> <buffer> <F8> :Sort<CR>

" " Autocommands
" " autosort
" augroup vimtodoSortOnSave
"     autocmd!
"     autocmd BufWritePost *todo 1,$call todo#Sort()
" augroup END

" Folding
" folding mechanism
function! todo#Fold(lnum)
    let line1 = getline(a:lnum)
    if line1 == ''
        return '>0'
    endif
    if line1 =~ '^-\s['
        return ">1"
    endif
    return "="
endfunction
setlocal foldexpr=todo#Fold(v:lnum)
setlocal foldmethod=expr

" folding text
function! todo#FoldText()
    return getline(v:foldstart) . ' '
endfunction
setlocal foldtext=todo#FoldText()
