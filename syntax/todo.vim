syn match    todoDone       /^\s*-\s\zs\[x\]\ze/
syn match    todoDelegated  /^\s*-\s\zs\[>\]\ze/
syn match    todoPending    /^\s*-\s\zs\[ \]\ze/
syn match    todoReference  /^\s\{6,}-\s\zs[^[].*\ze$/
syn region   todoInlineURL  start=/http[^ ]*/ end=/[ \t\n\r]/
syn match    todoComment    /\s\/\/\s.*/
syn match    todoTitle1     /^-\s\[[^[]\]\s.*:$/lc=6
syn match    todoTitle2     /^\s\s-\s\[[^[]\]\s.*:$/lc=8
syn match    todoTitle3     /^\s\s\s\s-\s\[[^[]\]\s.*:$/lc=10
hi! def link todoDone       Question
hi! def link todoDelegated  String
hi! def link todoPending    WarningMsg
hi! def link todoComment    Comment
hi! def link todoReference  Comment
hi! def link todoInlineURL  Comment
hi! def link todoTitle1     Title
hi! def link todoTitle2     Title
hi! def link todoTitle3     Title
